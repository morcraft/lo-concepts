const _ = require('lodash')
const renderConceptsCount = require('./renderConceptsCount.js')
const updateConcepts = require('./updateConcepts.js')
const showEarnedConceptsModal = require('./showEarnedConceptsModal.js')
const showNewConceptsModal = require('./showNewConceptsModal.js')
const refreshConceptsCount = require('./refreshConceptsCount.js')
const hideEarnedConceptsModal = require('./hideEarnedConceptsModal.js')
const props = require('./props.js')
const EventEmitter = require('events')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Invalid arguments. An object is expected.')

    if(!_.isObject(args.APIInstance))
        throw new Error('Invalid APIInstance.')

    if(!_.isObject(args.courseData))
        throw new Error('A courseData object property is expected.')
    
    var self = this

    self.props = new props(args)
    self.events = new EventEmitter()
    self.courseData = args.courseData
    self.APIInstance = args.APIInstance
    
    self.showEarnedConceptsModal = function(_args){
        return showEarnedConceptsModal({
            instance: self,
        })   
    }

    self.showNewConceptsModal = function(_args){
        return showNewConceptsModal(_.merge({
            instance: self
        }, _args))
    }

    self.hideEarnedConceptsModal = function(_args){
        return hideEarnedConceptsModal({
            instance: self,
        })   
    }

    self.renderConceptsCount = function(_args){
        return renderConceptsCount(_.merge({
            instance: self
        }, _args))
    }

    self.refreshConceptsCount = function(_args){
        return refreshConceptsCount(_.merge({
            instance: self
        }, _args))
    }

    self.updateConcepts = function(args){
        if(!_.isObject(args))
            throw new Error('Invalid arguments')
        
        return updateConcepts(_.merge({
            instance: self,
            APIInstance: self.APIInstance,
            courseData: self.courseData,
        }, args))
    }

    if(args.autoStart !== false){
        self.renderConceptsCount()
    }
}