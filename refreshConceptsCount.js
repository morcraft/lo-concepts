const _ = require('lodash')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Invalid arguments')

    if(!_.isObject(args.instance))
        throw new Error('Invalid instance')

    const size = _.size(args.instance.props.concepts)

    _.forEach(args.instance.props.elements.conceptsCount, function(element, key){
        element.innerHTML = size
    })

    args.instance.events.emit('countRefresh', {
        size: size
    })

    return true
}