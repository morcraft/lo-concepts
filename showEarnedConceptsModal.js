const _ = require('lodash')
const conceptsModal = require('./templates/conceptsModal.js')
const htmlToElement = require('./htmlToElement.js')

module.exports = function (args) {
    if (!_.isObject(args))
        throw new Error('Invalid arguments')

    if(!_.isObject(args.instance))
        throw new Error('Invalid loConceptsInstance')

    const html = conceptsModal({
        concepts: args.instance.props.concepts
    })

    const element = htmlToElement(html)
    args.instance.props.conceptsModalTarget.appendChild(element)
    args.instance.props.elements.earnedConceptsModal = element

    element.querySelector('.close-button').addEventListener('click', function(){
        return args.instance.hideEarnedConceptsModal(args)
    })

    args.instance.events.emit('conceptsModalShow', {
        element: element
    })
}