const _ = require('lodash')

module.exports = function (args) {
    if (!_.isObject(args))
        throw new Error('Invalid arguments')

    if(!_.isString(args.action))
        throw new Error('Invalid action')
    
    if (args.action === 'insert')
        return require('./insertConcepts.js')(args)
    
    if (args.action === 'delete')
        return require('./deleteConcepts.js')(args)

    throw new Error('Invalid action. Only insert and delete actions are supported')
}