const _ = require('lodash')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Invalid arguments')
    
    if(!(args.conceptsModalTarget instanceof Element))
        throw new Error('Invalid conceptsModalTarget')

    if(!(args.conceptsCountTarget instanceof Element))
    throw new Error('Invalid conceptsCountTarget')

    const self = this
    self.amount = 0
    self.enabled = true
    self.concepts = {}
    self.conceptsModalTarget = args.conceptsModalTarget
    self.conceptsCountTarget = args.conceptsCountTarget
    self.elements = {
        conceptsCount: []
    }
}