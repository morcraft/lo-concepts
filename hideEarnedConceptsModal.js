const _ = require('lodash')

module.exports = function (args) {
    if (!_.isObject(args))
        throw new Error('Invalid arguments')

    if(!_.isObject(args.instance))
        throw new Error('Invalid instance')

    const element = args.instance.props.elements.earnedConceptsModal
    
    if(!(element instanceof Element))
        return false

    element.parentNode.removeChild(element)
    args.instance.events.emit('conceptsModalHide', {
        element: element
    })
    
    return true
}