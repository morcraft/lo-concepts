const _ = require('lodash')
const swal = require('sweetalert2')
const newConceptsModal = require('./templates/newConceptsModal.js')

module.exports = function (args) {
    if (!_.isObject(args))
        throw new Error('Invalid arguments')

    if(!_.isObject(args.instance))
        throw new Error('Invalid instance')

    if(!_.isObject(args.concepts))
        throw new Error('Invalid concepts. An object is expected')

    if(!_.size(args.concepts))
        throw new Error('Empty object')

    if(!_.isString(args.title))
        throw new Error('Invalid title')

    if(!_.isString(args.subtitle))
        throw new Error('Invalid subtitle')

    const html = newConceptsModal(args)
    var modalClasses = ['lo-themed', 'concepts']
    if(_.isArray(args.classes)){
        _.forEach(args.classes, function(className){
            if(!_.isString(className))
                throw new Error('Invalid className in arguments')
            
            modalClasses.push(className)
        })
    }

    return swal({
        target: args.instance.props.conceptsModalTarget,
        html: html,
        customClass: modalClasses.join(' '),
        showConfirmButton: false,
        showCloseButton: true
    })
}